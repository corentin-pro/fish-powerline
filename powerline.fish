function fish_prompt
  set -l last_status $status
  set -l separator '\ue0b0'
  set -l reversed_separator '\ue0b2'

  set -l arch_icon '\uf303 '
  set -l home_icon '\uf015 '
  set -l folder_icon '\uf115 '
  set -l sub_folder_icon '\uf07c '
  set -l ok_icon '\uf00c '
  set -l fail_icon '\uf00d '

  set -l git_icon '\uf1d3 '
  set -l git_branch_icon '\uf126 '
  set -l git_untracked_icon '\uF059 '
  set -l git_unstaged_icon '\uF06A  '
  set -l git_staged_icon '\uF055 '
  set -l git_stash_icont '\uF01C '
  set -l git_incoming_changes_icon '\uF01A '
  set -l git_outgoing_changes_icon '\uF01B '
  set -l git_tag_icon '\uF02B '

  set -l background_jobs_icon '\uF013 '

  set -l initial_bg 31363b
  set -l initial_fg f67400
  set -l path_bg 1d99f3
  set -l git_clean_bg 11d116
  set -l git_dirty_bg f67400
  set -l normal_fg 31363b
  set -l ok_fg 11d116
  set -l error_bg ed1515
  set -l error_fg e0e010

  set -l initial_indicator (set_color -b $initial_bg)(set_color $initial_fg)" $arch_icon"(prompt_hostname)" "
  set -l initial_separator (set_color $initial_bg)(set_color -b $path_bg)"$separator "
  set -l cwd $sub_folder_icon(prompt_pwd)" "

  if test $last_status = 0
	set status_bg $initial_bg
	set status_fg $ok_fg
  else
	set status_bg $error_bg
	set status_fg $error_fg
  end

  if [ (_git_branch_name) ]
	set -l git_dirty (_is_git_dirty)
    if test -n $git_dirty
      set git_bg $git_dirty_bg
	else
      set git_bg $git_clean_bg
	end
	set cwd_separator (set_color $path_bg)(set_color -b $git_bg)"$separator "
    set git_info (set_color $normal_fg)"$git_icon $git_branch_icon "(_git_branch_name)
    if test -n $git_dirty
      set git_info "$git_info $git_unstaged_icon"
    end
	set git_separator (set_color $git_bg)(set_color -b $status_bg)"$separator "
  else
	set cwd_separator (set_color $path_bg)(set_color -b $status_bg)"$separator "
	set git_info ""
	set git_separator ""
  end

  # Notify if a command took more than 5 minutes
  if [ "$CMD_DURATION" -gt 300000 ]
    echo The last command took (math "$CMD_DURATION/1000") seconds.
  end

  if test $last_status = 0
    set status_info (set_color $status_fg)"$ok_icon "
  else
    set status_info (set_color $status_fg)"$fail_icon$last_status "
  end

  set -l status_indicator (set_color -b $status_bg)(set_color $status_fg)$status_info
  set -l status_separator (set_color normal)(set_color $status_bg)"$separator "(set_color normal)

  printf "$initial_indicator$initial_separator$cwd$cwd_separator$git_info$git_separator$status_indicator$status_separator\n> "
end

function _git_branch_name
  echo (command git symbolic-ref HEAD ^/dev/null | sed -e 's|^refs/heads/||')
end

function _is_git_dirty
  echo (command git status -s --ignore-submodules=dirty ^/dev/null)
end
