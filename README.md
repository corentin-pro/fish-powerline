# Fish-Powerline

Powerline prompt for fish shell/

## Install

Simply load the `powerline.fish` from fish config file (usually `~/.config/fish/config.fish`) with :

```
source [path]/powerline.fish
```
